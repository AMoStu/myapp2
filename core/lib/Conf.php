<?php
namespace core\lib;

class Conf
{
    static public $conf = array();

    // 获取单个配置项
    static public function get($name, $file)
    {
        /*
         * 1.引入配置文件
         * 2.缓存配置文件
         * */
        if(isset(self::$conf[$file])) {
            return self::$conf[$file][$name];
        } else {
            $path = ROOT . '/core/conf/' . $file . '.php';
            if(is_file($path)) {
                $conf = include $path;
                if(isset($conf[$name])) {
                    self::$conf[$file] = $conf;
                    return $conf[$name];
                } else {
                    throw new \Exception('配置项不存在' . $name);
                }

            } else {
                throw new \Exception('配置文件不存在' . $file);
            }
        }

    }


    // 获取这个配置文件的配置项
    static public function all($file)
    {
        /*
         * 1.引入配置文件
         * 2.缓存配置文件
         * */
        if(isset(self::$conf[$file])) {
            return self::$conf[$file];
        } else {
            $path = ROOT . '/core/conf/' . $file . '.php';
            if(is_file($path)) {
                $conf = include $path;
                return $conf;
            } else {
                throw new \Exception('配置文件不存在' . $file);
            }
        }

    }


}