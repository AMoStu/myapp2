<?php
namespace core\lib;

use core\lib\Conf;

class Route
{
    static public $controller;
    static public $action;

    public function __construct()
    {
        // 解析路由
        $uri = explode('/', $_SERVER['REQUEST_URI']);
        $name = explode('/', $_SERVER['SCRIPT_NAME']);
        $urlArr = array_diff_assoc($uri, $name);
        $controller = current($urlArr);
        $urlArr = array_splice($urlArr,1);
        $action = current($urlArr);
        $urlArr = array_splice($urlArr,1);

        // 提取控制器和方法
        if(empty($controller)){
            self::$controller = Conf::get('controller', 'route');
            self::$action = Conf::get('action', 'route');
        } else {
            self::$controller = $controller;
            if(empty($action)) {
                self::$action = Conf::get('action', 'route');
            } else {
                self::$action = $action;
            }
        }

        // 提取请求的get值
        $num = count($urlArr);
        $i = 0;
        while ($i < $num) {
            if(isset($urlArr[$i+1])) {
                $_GET[$urlArr[$i]] = $urlArr[$i+1];
            }
            $i = $i + 2;
        }


    }
}