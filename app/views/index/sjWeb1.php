<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>1001</title>
    <link rel="stylesheet" href="./front/bootstrap/bootstrap.min.css">
    <style>
        body{
            font-family:SourceHanSansCN-Regular;
        }
        #all-content{
            width: 100%;
            overflow: hidden;
            background-color: #F1F1F1;
            min-height: 100vh;
        }
        #match-content{
            min-height: 100vh;
            width: 1200px;
            margin: auto;
            background-color: white;
            overflow: hidden;
        }
        .real-content{
            padding-left: 263px;
        }
        .form-title{
            font-size:19px;
            font-weight:600;
            color:rgba(34,34,34,1);
            margin-top: 59px;
            margin-bottom: 62px;
        }
        .seconds-title{
            padding-left: 10px;
            border-left: 4px solid #222222;
            height: 18px;
            margin-bottom: 29px;
        }
        .input-class{
            line-height: 34px;
            margin-bottom: 22px;
        }
        .seconds-title>h6{
            margin: 0;
            font-size:15px;
            line-height: 18px;
            font-weight:600;
            color:rgba(34,34,34,1);

        }
        .unification{
            display: none;
        }
        .my-label{
            width: 190px;
            text-align: right;
        }
        .my-label>label{
            font-size:15px;
            font-weight:400;
            color:rgba(68,68,68,1);
        }
        .warning-input{
            border-color: #DE402B;
        }
        .must-tips{
            width: 10px;
            height: 10px;
        }
        .must-tips>img{
            width: auto;height: 100%;
        }
        .my-label>div{
            display: inline-block;vertical-align: top;
        }
        .my-input{
            width: 340px;
            height: 100%;

            overflow: hidden;
        }
        .form-bit{
            margin-bottom: 52px;
        }
        input{
            border: 1px solid #BBBBBB;
            outline:none;
        }
        .input-focus{
            border-color:#4D6DDB;
        }
        .hidden-img{

        }
        .my-input>input{
            width: 100%;height: 100%;
            border-radius:4px;
            padding-left: 20px;
        }
        .input-class>div{
            display: inline-block;vertical-align: top;
            height: 34px;
            line-height: 34px;
        }
        .readonly{
            border-color: #BBBBBB;
            background-color: #F2F2F2;
            color: #999999;
        }
        .tips-word{
            font-size:15px;
            font-weight:400;
            color:rgba(217,35,11,1);
            padding-left: 20px;
        }
        .label-readonly{
            color: #999999;
        }
        .my-button{
            display: inline-block;
            vertical-align: top;
            width:198px;
            height:50px;
        }
        .my-button>button{
            width: 100%;height: 100%;
            background:rgba(49,82,198,1);
            border-radius:4px;
            outline: none;
            border: none;
            font-size:18px;
            font-weight:400;
            color:rgba(255,255,255,1);
        }
        .my-button>button:hover{
            background-color: #5270D8;
        }
        .my-input-img{
            overflow: hidden;
            margin-right: 16px;
        }
        .add-img{
            width:139px;
            height:76px;
            display: inline-block;vertical-align: top;
            background:rgba(239,239,239,1);
            border:1px solid rgba(239,239,239,1);
            border-radius:4px;
            text-align: center;
            color:rgba(168,168,168,1);
            cursor: pointer;
        }
        .add-img:hover{
            border:1px solid rgba(77,109,219,1);
            color: #4D6DDB;
        }
        .add-img>div:first-child{
            line-height: 30px;
            margin-top: 10px;
            font-size: 28px;
        }
        .add-img>div{
            font-size:15px;
            font-weight:400;
            line-height: 16px;
        }
        .my-input-radio{
            padding-left: 4px;
            overflow: hidden;
        }
        .my-input-radio input{
            width: 0;height: 0;
        }
        .my-input-radio label{
            font-size:15px;
            font-weight:400;
            color:rgba(68,68,68,1);
            line-height: 10px;
        }
        .radio-group{
            margin-right: 30px;
        }
        .my-input-radio>div, .my-input-radio input{
            display: inline-block;vertical-align: top;
        }
        .my-radio{opacity: 0}
        .real-radio{
            padding-top: 10px;
            margin-right: 9px;
        }
        .real-radio span{
            display: inline-block;
        }
        .radio-check-border{
            height: 14px; width: 14px;
            border:2px solid rgba(187,187,187,1);
            border-radius:50%;
        }
        .radio-check{
           height: 10px;
            width: 10px;
            border-radius: 50%;
            vertical-align: top;
        }
    </style>
</head>
<body>
<div id="all-content">
    <div id="match-content">
        <div class="real-content">
            <div>
                <h2 class="form-title">1.请填写基本信息</h2>
                <div class="form-bit">
                    <div class="seconds-title"><h6>账号基本信息</h6></div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>入驻企业名称：</label>
                        </div>
                        <div class="my-input">
                            <input type="text"  class="my-verify" data-rule="require" name="companyName"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips"></div>
                            <label>登陆手机号：</label>
                        </div>
                        <div class="my-input">
                            <input  class="readonly" value="18829393838" readonly/>
                        </div>
                        <span class="tips-word label-readonly">默认为注册手机号</span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>请设置密码：</label>
                        </div>
                        <div class="my-input">
                            <input type="password"  value="" class="my-verify" data-rule="require" name="password"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>请重新输入密码：</label>
                        </div>
                        <div class="my-input">
                            <input type="password" value="" class="my-verify" data-rule="require compare" data-compare="password" name="antherPassword"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                </div>
                <div class="form-bit">
                    <div class="seconds-title"><h6>联系方式</h6></div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>业务联系人姓名：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" value="" name="contactsName" data-rule="require"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>业务联系人手机号：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" value="" name="contactsTel" data-rule="require tel"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>业务邮箱：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" value="" data-rule="require mail" name="contactsEmail"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                </div>
                <div>
                    <div class="my-label" style="display: inline-block">
                        <div class="must-tips"></div><label></label>
                    </div>
                    <div class="my-button">
                        <button>下一步</button>
                    </div>
                </div>
            </div>
            <div>
                <h2 class="form-title">2.请完善企业资质信息</h2>
                <div class="form-bit">
                    <div class="seconds-title"><h6>企业法定代表人基本信息</h6></div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>法定代表人姓名：</label>
                        </div>
                        <div class="my-input">
                            <input type="text"  class="my-verify" data-rule="require" name="legalName"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>法定代表人手机号：</label>
                        </div>
                        <div class="my-input">
                            <input type="text"  class="my-verify" data-rule="require tel" name="legalTel"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>法定代表人身份证号：</label>
                        </div>
                        <div class="my-input">
                            <input type="text"  class="my-verify" data-rule="require idcard" name="legalIdCard"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>法定代表人身份证像：</label>
                        </div>
                        <div class="my-input-img" style=" height: auto;">
                            <input type="file" style="display: none">
                            <input type="hidden" name="idCardBackImg" value="">
                            <div class="hidden-img">
                                <img src="">
                            </div>
                            <div class="add-img">
                                <div>+</div>
                                <div>上传正面</div>
                            </div>
                        </div>
                        <div class="my-input-img" style=" height: auto;">
                            <input type="file" style="display: none">
                            <input type="hidden" name="idCardFrontImg" value="">
                            <div class="add-img">
                                <div>+</div>
                                <div>上传反面</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-bit">
                    <div class="seconds-title"><h6>入驻企业信息</h6></div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>是否三证合一：</label>
                        </div>
                        <div class="my-input-radio">
                            <input type="hidden" class="my-radio" id="isture" value="0" name="isture"/>
                            <div class="radio-group" data-isture="1">
                                <label class="real-radio" >
                                    <span class="radio-check-border">
                                        <span class="radio-check"></span>
                                    </span>
                                </label>
                                <label>是</label>
                            </div>
                            <div class="radio-group" data-isture="0">
                                <label class="real-radio" >
                                    <span class="radio-check-border">
                                        <span class="radio-check" style="background-color: #3152C6"></span>
                                    </span>
                                </label>
                                <label>否</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-class ">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>公司名称：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="newCompanyName"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>公司经营地址：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="companyAddress"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class unification">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>统一社会信用证代码：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="companyAddress"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class disunion">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>营业执照注册号：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="licenseNumber"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class disunion">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>营业执照有效期：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="licenseValidDate"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>营业执照：</label>
                        </div>
                        <div class="my-input-img" style=" height: auto;">
                            <div class="add-img">
                                <div>+</div>
                                <div>点击上传</div>
                            </div>
                        </div>
                    </div>
                    <div class="input-class disunion">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>组织机构代码：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="organizingCode"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class disunion">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>组织机构代码证有效期：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="organizingValidDate"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class disunion">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>组织机构代码证：</label>
                        </div>
                        <div class="my-input-img" style=" height: auto;">
                            <div class="add-img">
                                <div>+</div>
                                <div>点击上传</div>
                            </div>
                        </div>
                    </div>
                    <div class="input-class disunion">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>纳税人识别码：</label>
                        </div>
                        <div class="my-input">
                            <input type="text" class="my-verify" data-rule="require" name="taxpayerNum"/>
                        </div>
                        <span class="tips-word"></span>
                    </div>
                    <div class="input-class disunion">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>税务登记证明：</label>
                        </div>
                        <div class="my-input-img" style=" height: auto;">
                            <div class="add-img">
                                <div>+</div>
                                <div>点击上传</div>
                            </div>
                        </div>
                    </div>
                    <div class="input-class">
                        <div class="my-label">
                            <div class="must-tips">
                                <img src="templete/testdirect/sj/must-tips@2x.png">
                            </div>
                            <label>开户许可证：</label>
                        </div>
                        <div class="my-input-img" style=" height: auto;">
                            <div class="add-img">
                                <div>+</div>
                                <div>点击上传</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 31px;">
                    <div class="my-label" style="display: inline-block">
                        <div class="must-tips"></div><label style="line-height: 50px;color: #3152C6;padding-right: 40px"><上一步</label>
                    </div>
                    <div class="my-button">
                        <button>提交审核</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../front/jquery/jquery.min1.9.1.js"></script>
<script src="../front/localResizeImg/lrz.bundle.js"></script>
<script>

    /**
     * 三证合一的 选择radio 按钮
     */
    $('.radio-group').click(function () {
        // 是否三证合一的样式
        $(this).find('.radio-check').css({
            backgroundColor:'#3152C6'
        });
        $(this).siblings('.radio-group').find('.radio-check').css({
            backgroundColor:'#FFFFFF'
        });
        //修改是否是三合一征的表单值
        var isTure = $(this).data('isture');
        if(isTure){
            $('#isture').val(1);
            $('.disunion').hide();
            $('.unification').show();
        }else{
            $('#isture').val(0);
            $('.disunion').show();
            $('.unification').hide();
        }
        //alert($('#isture').val());
    })

    /**
     * js验证规则
     */
    var formVerify = {
        rules:['tel','mail','require','compare','idcard'],
        verifyVal:'',
        currentElm:null, // jQuery 对象
        compareElem:null,// jQuery 对象
        errorMsg:{
            tel:'请输入正确的手机号',
            mail:'邮箱格式不正确',
            require:'不能为空',
            compare:'两次密码不一致',
            idcard:'身份证格式有误'
        },
        filterRules:function (ruleName) {
            var rs = false;
            switch (ruleName){
                case 'idcard':
                    rs = this.idcardRuleVerify();
                    break
                case 'tel':
                    rs = this.telRuleVerify();
                    break;
                case 'mail':
                    rs = this.mailRuleVerify();
                    break;
                case 'require':
                    rs = this.requireRuleVerify();
                    break;
                case 'compare':
                    rs = this.compareRuleVerify();
                    break;
                default :
            }
            return rs;
        },
        idcardRuleVerify:function(){
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(!reg.test(this.verifyVal)) {
                return  false;
            }else {
                return true;
            }
        },
        compareRuleVerify:function(){
            var compareElem = this.compareElem;
            var currentElm = this.currentElm;
            var compareVal = compareElem.val();
            compareElem.change(function () {
                currentElm.val('');
            });
            if(compareVal == this.verifyVal){
                return true;
            }else {
                return false;
            }
        },
        telRuleVerify: function () {
            var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
            if (!myreg.test(this.verifyVal)) {
                return false;
            } else {
                return true;
            }
        },
        mailRuleVerify: function () {
            var reg = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/; //正则表达式
            if(!reg.test(this.verifyVal)){ //正则验证不通过，格式不对
                return false;
            }else{
                return true;
            }
        },
        requireRuleVerify: function () {
            if(!this.verifyVal || this.verifyVal == "" || this.verifyVal == null || this.verifyVal == undefined){ //正则验证不通过，格式不对
                return false;
            }else{
                return true;
            }
        },
        getVerifyElm:function () {
            var _this = this;
            $('.my-verify').on("focus",function () {
                $(this).addClass("input-focus");
            });
            $('.my-verify').blur(function () {
                _this.currentElm = $(this);
                $(this).removeClass("input-focus");
                var ruleStr = $(this).data('rule');
                var rule = ruleStr.split(" ");
                _this.verifyVal = $(this).val();
                var ruleLenth = rule.length;
                var isPass = true;
                for (var i=0; i<ruleLenth;i++){
                    var ruleName = rule[i];
                    if(ruleName == "compare"){
                        _this.compareElem = $('input[name="'+$(this).data('compare')+'"]');
                    }
                    if(!_this.filterRules(rule[i])){ // 如果有一条为通过 则输出错误信息
                        _this.outputTips(this,ruleName);
                        isPass = false;
                        break;
                    }
                }
                if(isPass){
                    _this.restoreInputStyle(this);
                }
            });
        },
        outputTips:function (elm,ruleName) { // 输出错误信息
            $(elm).off("focus");
            $(elm).addClass('warning-input');
            $(elm).parent().siblings('.tips-word').html(this.errorMsg[ruleName]);

        },
        restoreInputStyle:function (elm) { // 验证都通过 恢复表单样式
            $(elm).removeClass('warning-input');
            $(elm).parent().siblings('.tips-word').html("");
            $(elm).on("focus",function () {
                $(this).addClass("input-focus");
            });
        }
    };
    formVerify.getVerifyElm();// 开启数据验证

    // 上传图片 请求
    var uploadImages = function(e){
        /* 压缩图片 */
        /*
         [options] 这个参数允许忽略

　　　　width {Number} 图片最大不超过的宽度，默认为原图宽度，高度不设定时会适应宽度

　　　　height {Number} 图片的最大高度，默认为原图高度

　　　　quality {Number} 图片压缩质量，取值 0 - 1，默认为 0.7

　　　　fieldName {String} 后端接收的字段名，默认：file
        */
        lrz(this.files[0], {
            //width: 300 //设置压缩参数
        }).then(function (rst) {
            /* 处理成功后执行 */
            console.log(rst.base64);return false;
            rst.formData.append('data', rst.base64); // 添加额外参数
            $.ajax({
                url: "?r=order/uploadImage",
                type: "POST",
                data: rst.formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    var data = JSON.parse(data);
                    if(data.result == 1){
                        isSuccessNum++;
                        var goodid = $('#avatarSlect').data('id');
                        var imgsElm = $('#imgs' + goodid);
                        var imgOutlaye = imgsElm.find('.reason-img').eq(0).clone();
                        imgOutlaye.find('.del-images').css('display','inline-block');
                        imgOutlaye.removeClass('add-img').find('.user-goods-img').attr('src',data.imgSrc).data('relpath',data.relativePath);
                        imgsElm.find('ul').prepend(imgOutlaye);
                    }else {
                        isCloseNum++;
                    }
                }
            });
        }).catch(function (err) {
            /* 处理失败后执行 */
        }).always(function () {
            /* 必然执行 */
        });
    };
    $('input[type="file"]').change(uploadImages);
    $('.add-img').click(function () {
        $(this).siblings('input[type="file"]').click();
    });
</script>
</body>
</html>