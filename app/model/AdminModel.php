<?php
namespace app\model;

use core\lib\Model;

class AdminModel extends Model
{
    public $table = 'admin';

    public function lists()
    {
        $table = $this->table;
        $where = array('id'=>1);

        $rs = $this->select($table, '*', $where);

        return $rs;
    }
}