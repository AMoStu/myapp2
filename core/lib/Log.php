<?php
namespace core\lib;

use core\lib\Conf;
class Log
{
    static public $driver;

    static public function init()
    {
        $conf = Conf::get('driver', 'log');
        $conf = '\core\lib\driver\\' . $conf;
        self::$driver = new $conf();
    }

    static public function log($message, $file = 'log'){
        self::$driver->log($message, $file);
    }


}