<?php
/**
 * 入口文件
 * 1.定义常量
 * 2.引入函数库
 * 3.启动框架
 */

/*定义常量*/
define('ROOT', realpath('./'));

//echo ROOT;
define('CORE', ROOT . '/core'); // 框架核心文件路径
define('APP', ROOT . '/app'); // 项目文件路径
define('MODULE', '/app'); // 项目文件路径
define('DEBUG', true);

// 加入composer的自动加载类
include 'vendor/autoload.php';

if(DEBUG) { // 如果开启调试模式，输出所有错误
    // 错误提示插件
    $whoops = new \Whoops\Run();
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();

    ini_set('display_error',  'On');
}else{ // 没有开启调试模式， 禁用所有错误输出
    ini_set('display_error', 'Off');
}

include CORE . '/common/function.php'; /*引入函数库*/
include CORE . '/Core.php'; // 引入核心类

spl_autoload_register('\core\Core::load');
\core\Core::run();




