<?php
namespace core\lib\driver;
use \core\lib\Conf;
class File
{
    public $path;
    public function __construct() {
        $conf = Conf::get('option', 'log');
        $this->path = $conf['path'];
    }

    public function log($message, $file = 'log'){

        $_path = $this->path . '/' . date('Ymd');

        if(!is_dir($this->path)) {
            mkdir($this->path, 0777, true);
        }

        if(!is_dir($_path)){
            mkdir($_path, 0777, true);
        }

        // 写入日志
        file_put_contents($_path . '/' . $file . '.php', date('Ymd H:i:s') . '  ' . json_encode($message) . PHP_EOL, FILE_APPEND );

        return true;

    }
}