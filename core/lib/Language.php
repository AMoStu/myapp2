<?php
namespace core\lib;

class Language
{
    static public $language = array();

    // 获取单个言语包
    static public function get($name, $file)
    {
        /*
         * 1.引入语言包文件
         * 2.缓存语言包文件
         * */
        if(isset(self::$language[$file])) {
            return self::$language[$file][$name];
        } else {
            $path = ROOT . '/core/language/' . $file . '.php';
            if(is_file($path)) {
                $language = include $path;
                if(isset($language[$name])) {
                    self::$language[$file] = $language;
                    return $language[$name];
                } else {
                    throw new \Exception('言语包不存在' . $name);
                }

            } else {
                throw new \Exception('言语包文件不存在' . $file);
            }
        }

    }


    // 获取整个语言文件的配置项
    static public function all($file)
    {
        /*
         * 1.引入语言包文件
         * */
        if(isset(self::$language[$file])) {
            return self::$language[$file];
        } else {
            $path = ROOT . '/core/language/' . $file . '.php';
            if(is_file($path)) {
                $language = include $path;
                return $language;
            } else {
                throw new \Exception('言语包文件不存在' . $file);
            }
        }

    }


}