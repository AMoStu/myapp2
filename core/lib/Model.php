<?php
namespace core\lib;

use core\lib\Conf;
class Model extends \Medoo\Medoo
{
    public function __construct()
    {
        $option = Conf::all('databases');
       // 使用pdo 操作数据库
        /* try{
            parent::__construct($Conf['dsn'], $Conf['username'], $Conf['passwd']);
        }catch (\PDOException $e){
            echo $e->getMessage();
        }*/

        // 使用medoo操作数据库
        parent::__construct($option);

    }
}